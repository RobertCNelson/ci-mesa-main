# ci-mesa-main


# Add repo to your apt sources.list

```
deb [trusted=yes] https://beagleboard.beagleboard.io/ci-mesa-main stable main
```

# Quick One line:

```
sudo sh -c "echo 'deb [trusted=yes] https://beagleboard.beagleboard.io/ci-mesa-main stable main' > /etc/apt/sources.list.d/ci-mesa.list"
```

#

